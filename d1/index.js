console.log('Hello World');

//Loops

//While Loops

/*
Syntax:
	while(expression/condition) {
		statement
	}
*/

/*Create a function called displayMsgToSelf()
	display a message to your past self in your console 10 times
	invoke the function 10 times
*/

function displayMsgToSelf() {
	console.log('Dont text her back.');
}

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();

//while loop
//will allow us to repeat an action or an instruction as long as the condition is true

let count = 10;

while (count !== 0) {
	console.log('Dont text her back.')
	count--;
}

/*
1st loop - count 10
2nd loop - 9
3rd loop - 8
4th loop - 7
5th loop - 6
6th loop - 5
7th loop - 4
8th loop - 3
9th loop - 2
10th loop - 1
0 - 0 = stop
if theres no decrementation, the condition is always true, thus an infinite loop.
infinite loops will run your code block forever until you stop it.
*/

let count1 = 5;

while(count1 !==0) {
	console.log(count1);
	count1--;
}

//Mini activity
//The ff while loop should display the numbers from 1-5 (in order);
console.log('Mini activity');
let count2 = 1;

while(count2 <= 5) {
	console.log(count2);
	count2++;
}

/*
Do while loop
	is like a while, but do-while loops guarantee that the code
	will be executed atleast once.
Syntax:
	do {
		statement
	}while(expression/condition)
*/
console.log('Do while example');

let doWhileCounter = 1;
do {
	console.log(doWhileCounter);
	doWhileCounter++;
}while (doWhileCounter <= 20);

// ----------
console.log('another example');
let number = Number(prompt('Give me a number'));

do {
	console.log("Do while: " + number);

	// Increase the value of number by 1 after every iteration to stop the loop when it reaches 10
	// number = number + 1;
	number += 1;
}while (number <= 10);


// For Loop
console.log('For Loop');
/*
it consists of three parts:
1. initialization - value that will track the progression of the loop.
2. expression/condition that will be evaluated which will determine whether the loop will run one more time.
3. finalExpression indeicates how to advance the loop (++, --)

syntax:
	for(initialization; expression/condition; finalExpression) {
		statement
	}
*/

// loop from 0-20
for (let count = 0; count <= 20; count++) {
	console.log(count);
}

//For loops accessing array Items

let fruits = ["Apple", "Durian", "Kiwi", "Pineapple", "Mango", "Orange"];

console.log(fruits[2]);
// .length property is also a property of an array that shows the total number of items in an array
console.log(fruits.length); //5 total number of items in array
// a more reliable way of checking the last item in an array:
// arrayName[arrayName.length-1]
console.log(fruits[fruits.length-1]); //orange

//show all items in an array in the console using loops:

for(let index = 0; index < fruits.length; index++) {
	console.log(fruits[index]);
}

//Mini Activity
console.log('Mini activity');
//create an array with atleast 6 items
	//each item as one of your fav countries
	//display all items in the console EXCEPT for the last item

let faveCountry = ["Philippines", "Japan", "USA", "Iceland", "Thailand", "Dubai"]

for(let index =0; index < faveCountry.length-1; index++) {
	console.log(faveCountry[index])
}

//for loops accessing elements of a string

let myString = "alex";
//.lenth it is also a property used in strings
console.log(myString.length); //4

//individual characters of a string can be access using its index number. and it starts with 0 - nth number.
console.log(myString[0]);

//will create a loop that will print out the individual letters of the myString variable
for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}


// --------------------------------
console.log('another example');

let myName = "Jane";

/*
create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel

*/

for(let i = 0; i < myName.length; i++) {
	//if the character of your name is a vowel letter, display number "3"
	//console.log(myName[i]);
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		) {
		console.log(3);
	} else {
		//Print in all the non-vowel in the console
		console.log(myName[i])
	}

}

//Continue and Break Statements
console.log('Continue and Break Statements');
//continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block({})
//break statement is used to terminate the current loop once a match has been found

for(let count = 0; count <= 20; count++) {
	//if remainder is equal to 0, use continue statement
	if(count % 2 === 0) {
		//tells the code to continue to the next iteration
		//this ignores all statement located after the continue statement
		continue;
	}
	//the current value of a number is printed out if remainder is not equal to 0
	console.log("Continue and Break: " + count);

	//if the current value of count is greater than 10, then use break.
	if(count > 10) {
		break;
	}

}



console.log('another example of Continue and Break')
//create a loop that will iterate based on the length of the string
let name = "alexandro";
for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	//if the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	} if(name[i] == "d") {
		break;
	}
}


//---------- miss thonie Friday March 11, 2022 ---------------
//counting from 1-10
console.log('Counting from 1-10');

for (let i = 1; i <= 10; i++) {
	console.log(i);
}

// while - counting from 1-10
let n = 1;

while (n <= 10) {
	console.log(n);
	n++;
}

// do while loop - similar to a normal while loop except
// that the block of code being lopped over is guaranteed to run at least once.

let doritos = 100;

do {
	console.log('With ' + doritos + " Doritos left, I can eat.")
		doritos -= 20;
}while (doritos > 0);
console.log('My doritos are gone now.')

// for in

let colors = ['red', 'pink', 'yellow'];
for (let x in colors) {
	console.log(colors[x]);
}

let person = {
	name: "Tom",
	age: 40
};

for (let data in person) {
	console.log("This person's " + data + " is " + person[data]);
	console.log("This person's " + data + " is " + person[data]);
}


//Mini activity --- remove vowels from a long word

let string = prompt('Enter word:');
for (let q = 0; q < string.length; q++) {
	if (string[q] != 'a' && string[q] != 'e' && string[q] != 'i' && string[q] != 'o' && string[q] != 'u') {
		console.log(string[q]);
	}
}